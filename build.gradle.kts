import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val springbootVersion: String by project
val springVersion: String by project
val kotlinVersion: String by project

plugins {
  id("org.springframework.boot") version "2.6.2"
  id("io.spring.dependency-management") version "1.0.11.RELEASE"

  kotlin("jvm")
  kotlin("plugin.spring")
  kotlin("plugin.serialization")
}

group = "com.fvpaz"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
  mavenCentral()
}

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-webflux")
  implementation("org.springframework.boot:spring-boot-starter-security")

  implementation("org.springframework.boot:spring-boot-starter-data-redis")
//  implementation("org.springframework.boot:spring-session-data-redis")
  implementation("org.springframework.session:spring-session-data-redis")
  implementation("it.ozimov:embedded-redis:0.7.3") {
//    exclude("ch.qos.logback","logback-core")
//    exclude("ch.qos.logback","logback-classic")
    exclude("commons-logging", "commons-logging")
    exclude("org.slf4j", "slf4j-simple")
  }


  implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

//  implementation("org.springdoc:springdoc-openapi-ui:1.6.4")

  implementation("org.litote.kmongo:kmongo-coroutine:4.4.0")
  implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
  implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
  implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
  runtimeOnly("org.litote.kmongo:kmongo-coroutine-serialization:4.4.0")
  implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")

  implementation("com.sendgrid:sendgrid-java:4.8.1")
  compileOnly("javax.servlet:javax.servlet-api:4.0.1")


  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("io.projectreactor:reactor-test")
  testImplementation("org.springframework.security:spring-security-test")
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "17"
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}
