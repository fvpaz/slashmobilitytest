### Requisitos

-----
* Tener una cuenta de SendGrid configurada para realizar llamadas a su API.
* MongoDB 4.2 o superior con **replication Data** (Transactions)
* Servidor Redis para almacenar la session del Usuario.
> Existe un servidor embebido **REDIS** en el proyecto para hacer las pruebas.


**MongoDB activar replication Data:**
> mongod --port 27017 --dbpath ${MongoData} --replSet rs1 --bind_ip localhost


**Redis arrancar el servidor:**
```kotlin
fun initRedis() {
  val redisServer = RedisServer.builder()
    .port( 6379)
    .setting("bind 127.0.0.1")
    .setting("maxmemory 128M").build()
  if (redisServer.isActive)
    redisServer.stop()

  redisServer.start()
}
```

### MongoDB script for create DB data

----

```kotlin
// Create Collections
db.createCollection("users")
db.createCollection("authData")
db.createCollection("products")
db.createCollection("companies")

//Add unique index
db.authData.createIndex( { "user": 1 }, { unique: true } )
db.users.createIndex( { "codUser": 1 }, { unique: true } )
db.users.createIndex( { "codProduct": 1 }, { unique: true } )
db.users.createIndex( { "codCompany": 1 }, { unique: true } )
db.users.createIndex( { "name": 1 }, { unique: true } )

// Insert Data
db.products.insertOne([
  {
    "codProduct": "x3991",
    "company": "Milan",
    "desc": "",
    "imagen": "",
    "name": "boligrafo",
    "type": "escolar"
  }
])
db.products.insertOne(
  {
    "codProduct": "x3992",
    "company": "Milan",
    "desc": "",
    "imagen": "",
    "name": "goma de borrar",
    "type": "escolar"
  }
)
db.products.insertOne({
  "codProduct": "x3912",
  "company": "Roca",
  "desc": "",
  "imagen": "",
  "name": "Taza de vater",
  "type": "lavabo"
})
db.products.insertOne({
  "codProduct": "x3913",
  "company": "Roca",
  "desc": "",
  "imagen": "",
  "name": "ducha",
  "type": "lavabo"
})

db.users.insertOne(
  {
    "_id": "61de32fc2730ea3c39e93acc",
    "codUser": "XS431",
    "email": "Andrey@gmalk.cos",
    "name": "juan",
    "surname": "",
    "validate": "FALSE"
  }
)
db.authData.insertOne(
  {
    "password": "and123rey",
    "user": "Andrey",
    "userID": "61de32fc2730ea3c39e93acc"
  }
)

db.companies.insertMany([{
  "address": "Calle sdlod",
  "city": "roma",
  "codCompany": "X12902",
  "name": "Milan",
  "phone": "4949390"
},
  {
    "address": "Calle PWOE",
    "city": "madrid",
    "codCompany": "LPE2R",
    "name": "ROCA",
    "phone": "9390840"
  },
  {
    "address": "7 avenida",
    "city": "Georgia",
    "codCompany": "slo23",
    "name": "McDonalds",
    "phone": "4949390"
  }
])

```

### Pruebas o Testing

----

* Arrancar el **Servidor Web** y **REDIS** a traves de la classe **App.kt** de Spring.
* Realizar las llamadas a traves de postman a los webServices. (Se adjuntan las pruebas dentro de la carpeta **Postman** del proyecto).
* Se adjunta la documentacion API dentro de la carpeta **OpenAPI** del proyecto.