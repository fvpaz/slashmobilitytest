package com.fvpaz.slashmobilitytest

import com.fvpaz.slashmobilitytest.model.redis.SessionEntity
import com.fvpaz.slashmobilitytest.model.redis.repository.SessionRepo
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = arrayOf(RedisConfigurationTest::class))
class SlashmobilityTestApplicationTests(val sessionRepo: SessionRepo) {

  @Test
  fun saveUserToRedis() {
    val id = UUID.randomUUID()
    val sessionEntity = SessionEntity(id,"juan")
//    val sessionRepository = SessionRepository()
      val result = sessionRepo.save(sessionEntity)

    assertNotNull(result)
  }

}
