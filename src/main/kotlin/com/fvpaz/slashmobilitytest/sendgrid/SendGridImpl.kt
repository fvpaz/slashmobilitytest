package com.fvpaz.slashmobilitytest.sendgrid

import com.fvpaz.slashmobilitytest.controllers.ResponseData
import com.fvpaz.slashmobilitytest.controllers.Status
import com.sendgrid.Method
import com.sendgrid.Request
import com.sendgrid.SendGrid
import com.sendgrid.helpers.mail.Mail
import com.sendgrid.helpers.mail.objects.Email
import com.sendgrid.helpers.mail.objects.Personalization
import org.springframework.http.HttpStatus
import java.io.IOException


private val EMAIL_COMPANY = "YOUR_EMAIL_COMPANY" // replace with your email
private val TEMPLATE_ID = "TEMPLATE_ID OF YOUR DYNAMIC_TEMPLATE" // replace with your email


class SendGridImpl {
  private fun buildDynamicTemplate(toEmail: Email): Personalization {
    val personalization = Personalization()
    personalization.addDynamicTemplateData("name", "UserVerication") //Template name
    personalization.addTo(toEmail)
    return personalization
  }

  fun sendMail(emailUser: String): ResponseData {
    val from = Email(EMAIL_COMPANY)
    val toEmail = Email(emailUser)

    val personalization = buildDynamicTemplate(toEmail)

    val mail = Mail()
    mail.setFrom(from)
    mail.addPersonalization(personalization)
    mail.setTemplateId(TEMPLATE_ID)

    val sg = SendGrid(System.getenv("SENDGRID_API_KEY"))
    try {
      val request = Request()
      request.setMethod(Method.POST)
      request.setEndpoint("mail/send")
      request.setBody(mail.build())
      val response = sg.api(request)

      response.let {
        val httpCode = it.statusCode
        val codeVal = "HTTPCODE - ${httpCode}"
        return if (httpCode.equals(HttpStatus.OK))
          ResponseData(Status.SUCCESS, code = codeVal, desc = it.body)
        else
          ResponseData(Status.FAIL, code = codeVal, desc = it.body)
      }

    } catch (ex: IOException) {
      val msgErr = ex.message ?: "Fallo al realizar la llamada a la API de SENDGRID"
      return ResponseData(Status.FAIL, desc = msgErr)
    }
  }
}