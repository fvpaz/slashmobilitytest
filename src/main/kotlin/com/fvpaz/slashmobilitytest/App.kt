package com.fvpaz.slashmobilitytest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import redis.embedded.RedisServer

@SpringBootApplication
class App

fun main(args: Array<String>) {
  initRedis()
  runApplication<App>(*args)
}

fun initRedis() {
  val redisServer = RedisServer.builder()
    .port( 6379)
    .setting("bind 127.0.0.1")
    .setting("maxmemory 128M").build()
  if (redisServer.isActive)
    redisServer.stop()

  redisServer.start()
}
