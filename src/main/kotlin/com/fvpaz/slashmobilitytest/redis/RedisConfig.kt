package com.fvpaz.slashmobilitytest.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories


@Configuration
@EnableRedisRepositories
class RedisConfig {
  @Bean
  fun redisConnectionFactory(redisProperties: RedisProps): LettuceConnectionFactory? {
    return LettuceConnectionFactory(
      redisProperties.redisHost,
      redisProperties.redisPort
    )
  }

  @Bean
  fun redisTemplate(connectionFactory: LettuceConnectionFactory): RedisTemplate<*, *> {
    val template = RedisTemplate<ByteArray, ByteArray>()
    template.setConnectionFactory(connectionFactory)
    return template
  }
}