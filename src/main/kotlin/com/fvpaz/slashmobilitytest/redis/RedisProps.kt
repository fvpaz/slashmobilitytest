package com.fvpaz.slashmobilitytest.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import kotlin.properties.Delegates

@Configuration
class RedisProps {
  var redisPort by Delegates.notNull<Int>()
  lateinit var redisHost: String

  fun RedisProperties( @Value("\${spring.redis.port}") redisPort: Int,
                       @Value("\${spring.redis.host}") redisHost: String ) {
    this.redisPort = redisPort
    this.redisHost = redisHost
  }
}