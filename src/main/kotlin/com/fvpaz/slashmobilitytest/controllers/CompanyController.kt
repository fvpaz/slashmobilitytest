package com.fvpaz.slashmobilitytest.controllers

import com.fvpaz.slashmobilitytest.beans.Company
import com.fvpaz.slashmobilitytest.model.CompanyDAO
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.Serializable
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.*

@Serializable
data class CompanyEdit(
  var codCompany: String,
  var newcodCompany: String,
  var name: String,
  var address: String,
  var phone: String,
  var city: String
)

@RestController
class CompanyController {
  @FlowPreview
  @GetMapping("/listCompanies", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  fun listCompanies(): ResponseEntity<Flow<Company>> {
    val companies = CompanyDAO.listCompanies()

    return ok(companies)
  }

  @GetMapping("/getCompany", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun getCompany(@RequestParam codCompany: String): ResponseEntity<Any> {
    val company = CompanyDAO.getCompanyByCode(codCompany)
    company ?: return ok(ResponseData(Status.FAIL, desc = "Fail DB - No se ha encontrado la compañia."))

    return ok(company)
  }

  @PostMapping("/editCompany", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun editCompany(@RequestBody companyEdit: CompanyEdit): ResponseEntity<Any> {
    if (companyEdit.codCompany.isBlank())
      return ok(ResponseData(Status.FAIL, desc = "El parametro codCompany es obligatorio"))

    val companyDB = CompanyDAO.getCompanyByCode(companyEdit.codCompany)
    companyDB ?: return ok(ResponseData(Status.FAIL, desc = "Fail DB - No se ha encontrado la compañia."))

    if (companyEdit.newcodCompany.isBlank())
      companyEdit.newcodCompany = companyDB.codCompany

    if (companyEdit.name.isBlank())
    companyEdit.name = companyDB.name

    if (companyEdit.address.isBlank())
      companyEdit.address = companyDB.address

    if (companyEdit.phone.isBlank())
      companyEdit.phone = companyDB.phone

    if (companyEdit.city.isBlank())
      companyEdit.city = companyDB.city

    val updateResult = CompanyDAO.editCompany(companyDB.codCompany, companyEdit)

    val codCompany = companyEdit.newcodCompany
    return if (updateResult.modifiedCount > 0)
      ok(ResponseData(Status.OK, desc = "La compañia: $codCompany ha sido modificado"))
    else
      ok(ResponseData(Status.FAIL, desc = "Fail DB - No se ha podido modificar los datos de la compañia: $codCompany"))
  }
}