package com.fvpaz.slashmobilitytest.controllers

import com.fvpaz.slashmobilitytest.beans.Auth
import com.fvpaz.slashmobilitytest.beans.User
import com.fvpaz.slashmobilitytest.beans.UserRequest
import com.fvpaz.slashmobilitytest.beans.restorPswdRequest
import com.fvpaz.slashmobilitytest.model.RegisterTransaction
import com.fvpaz.slashmobilitytest.model.UserDAO
import com.fvpaz.slashmobilitytest.sendgrid.SendGridImpl
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.Serializable
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.*

@Serializable
data class ResponseData(val status: Status, var code: String = "", var desc: String = "")

enum class Status(val estado: String) { OK("OK"), SUCCESS("SUCCESS"), FAIL("FAIL") }

@RestController
class UserController {
  @PostMapping("register", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun register(@RequestBody userRequest: UserRequest): ResponseData {
    val registerTransaction = RegisterTransaction()

    val userName = userRequest.user
    val password = userRequest.password

    val codUser = userRequest.codUser
    val email = userRequest.email
    val name = userRequest.name
    val surname = userRequest.surname

    val user = User(codUser, email, name, surname)
    val auth = Auth(userName, password)

    val registerResult = registerTransaction.registerUser(user, auth)

    if (registerResult.status == Status.FAIL) return registerResult

    val sendGrid = SendGridImpl()
    val mailValidateResult = sendGrid.sendMail(user.email)

    if (mailValidateResult.status == Status.FAIL)
      return mailValidateResult

    return ResponseData(Status.SUCCESS, desc = "Usuario Registrado correctamente.")
  }

  @GetMapping("validateEmail", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun validateEmail(@RequestParam codUser: String): ResponseEntity<ResponseData> {

    val user = UserDAO.getUserbyCode(codUser)
    user ?: return ok(ResponseData(Status.FAIL, desc = "Fail DB - No se ha encontrado el usuario."))

    if (user.validate.equals("TRUE")) return ok(ResponseData(Status.FAIL, desc = "El usuario ya tiene validado su usuario."))


    val usuario = UserDAO.getUserbyCode(codUser)
    val updateResult = UserDAO.validateUserByCode(codUser)

    return if (updateResult.modifiedCount > 0) ok(ResponseData(Status.SUCCESS, desc = "El usuario: $codUser se ha podido validarse."))
    else ok(ResponseData(Status.FAIL, desc = "El usuario: $codUser no se ha podido validar."))
  }

  @PostMapping(
    "makeLogin", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE)
  )
  suspend fun authLogin(@RequestBody auth: Auth): ResponseEntity<ResponseData> {
    val username = auth.username

    val authData = UserDAO.getAuthData(username)
    authData ?: return ok(ResponseData(Status.FAIL, desc = "Fail DB - El usuario no existe."))

    val userID = authData.userID
    val user = UserDAO.getUserbyID(userID)
    user ?: return ok(ResponseData(Status.FAIL, desc = "Fail DB - El usuario no tiene configurado sus datos."))

    if (!user.validate.equals("TRUE")) {
      val responseData = ResponseData(Status.FAIL, desc = "Fail DB - El usuario no esta validado se ha enviado un email de validacion.")
      val sendGrid = SendGridImpl()
      val mailValidateResult = sendGrid.sendMail(user.email)

      if (mailValidateResult.status == Status.FAIL) responseData.desc = "Fallo al enviar email de Validacion - ${mailValidateResult.desc}"

      return ok(responseData)
    }

    //TODO create session and check session
//    val session = request.getSession()

    return ok(ResponseData(Status.OK, desc = "Se ha realizado el login correctamente"))
  }

  @GetMapping("makelogout", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  fun authLogout(): ResponseEntity<ResponseData> {
//, @RequestParam sessionID: String
//    val session = request.getSession().invalidate()
//    println(session.)
    return ok(ResponseData(Status.OK, "Se ha cerrado la session del usuario correctamente"))
  }

  @PostMapping("restorePswd", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun restorePswd(@RequestBody bodyRequest: restorPswdRequest): ResponseEntity<ResponseData> { //TODO validate account for email
    val username = bodyRequest.username
    val newPswd = bodyRequest.newPswd

    val authData = UserDAO.getAuthData(username)
    authData ?: return ok(ResponseData(Status.FAIL, desc = "Fail DB - El usuario no existe."))

    val updatePswdResult = UserDAO.updatePswdByUsername(username, newPswd)
    return if (updatePswdResult.modifiedCount > 0)
      ok(ResponseData(Status.OK, "Se ha modificado correctamente la contraseña"))
    else
      ok(ResponseData(Status.FAIL, desc = "Fail DB -No se ha podido resetear la contraseña del usuario: $username"))
  }

  @FlowPreview
  @GetMapping("listUsers", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  fun listUsers(): ResponseEntity<Flow<User>> {
    val users = UserDAO.listUsers()

    return ok(users)
  }

  @GetMapping("getUser/{codUser}", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun getUserByID(@PathVariable("codUser") codUser: String): ResponseEntity<Any> {

    val usuario = UserDAO.getUserbyCode(codUser)
    usuario ?: return ok(ResponseData(Status.FAIL, desc = "Fail DB - El usuario no existe."))

    return ok(usuario)
  }
}
