package com.fvpaz.slashmobilitytest.controllers

import com.fvpaz.slashmobilitytest.beans.Product
import com.fvpaz.slashmobilitytest.model.ProductDAO
import com.fvpaz.slashmobilitytest.model.UserDAO
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.Serializable
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.*

@Serializable
data class ProductEdit(
  var codProduct: String,
  var newCodeProduct: String,
  var name: String,
  var type: String,
  var desc: String = "",
  var imagen: String = "",
  var company: String
)

@RestController
class ProductController {
  @FlowPreview
  @PostMapping("/editProduct",consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun editProduct(@RequestBody productEdit: ProductEdit): ResponseEntity<Any> {
    if (productEdit.codProduct.isBlank())
      return ok(ResponseData(Status.FAIL, desc = "El parametro codProduct es obligatorio"))

    val productDB = ProductDAO.getProductByCode(productEdit.codProduct)
    productDB ?: return ok(ResponseData(Status.FAIL, desc = "Producto no se ha encontrado"))


    if (productEdit.newCodeProduct.isBlank())
      productEdit.newCodeProduct = productDB.codProduct

    if (productEdit.name.isBlank())
      productEdit.name = productDB.name

    if (productEdit.type.isBlank())
      productEdit.type = productDB.type

    if (productEdit.desc.isBlank())
      productEdit.desc = productDB.desc

    if (productEdit.imagen.isBlank())
      productEdit.imagen = productDB.imagen

    if (productEdit.company.isBlank())
      productEdit.company = productDB.company

    val updateResult = ProductDAO.editProduct(productDB.codProduct, productEdit)

    val codProduct = productEdit.newCodeProduct
    return if (updateResult.modifiedCount > 0)
      ok(ResponseData(Status.OK, desc = "El producto: $codProduct ha sido modificado"))
    else
      ok(ResponseData(Status.FAIL, desc = "Fail DB - No se ha podido modificar los datos del producto: $codProduct"))
  }

  @FlowPreview
  @GetMapping("/listProducts")
  suspend fun listProducts(): ResponseEntity<Flow<Product>> {
    val products = ProductDAO.listProducts()

    return ok(products);
  }

  @FlowPreview
  @GetMapping("/listProducts", params = arrayOf("type") , produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun listProductsByType(@RequestParam type: String): ResponseEntity<Flow<Product>> {
    val products = ProductDAO.listProductsByType(type)

    return ok(products);
  }

  @FlowPreview
  @GetMapping("/listProducts", params = arrayOf("city"), produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun listProductsByCity(@RequestParam city: String): ResponseEntity<Flow<Any>> {
    val products = ProductDAO.listProductsByCity(city)

    return ok(products);
  }

  @GetMapping("/getProduct", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
  suspend fun getProductById(@RequestParam codProduct: String): ResponseEntity<Any> {
    val product = ProductDAO.getProductByCode(codProduct)

    product ?: ok(ResponseData(Status.FAIL, desc = "Producto no encontrado"))

    return ok(product)
  }
}