package com.fvpaz.slashmobilitytest.model

import com.fvpaz.slashmobilitytest.beans.Company
import com.fvpaz.slashmobilitytest.controllers.CompanyEdit
import com.fvpaz.slashmobilitytest.model.DbConstants.COMPANY_COLLECTION
import com.mongodb.client.result.InsertOneResult
import com.mongodb.client.result.UpdateResult
import kotlinx.coroutines.flow.Flow
import org.litote.kmongo.MongoOperator.*
import org.litote.kmongo.formatJson

object CompanyDAO {
  private val connection = Connection()
  private val companyCollection = connection.getCollection<Company>(COMPANY_COLLECTION)

  suspend fun insertCompany(company: Company): InsertOneResult {
    return companyCollection.insertOne(company)
  }

  fun listCompanies(): Flow<Company> {
    return companyCollection.find().toFlow()
  }

  suspend fun getCompanyByCode(codCompany: String): Company? {
    return companyCollection.findOne("{codCompany: '$codCompany' }");
  }

  suspend fun editCompany(codCompany: String, companyEdit: CompanyEdit): UpdateResult {
    val newName = companyEdit.name
    val newAddress = companyEdit.address
    val newCity = companyEdit.city
    val newPhone = companyEdit.phone

    val editProductBson = """
      
    """.formatJson()

    return companyCollection.updateOne(
      "{codCompany: '$codCompany'}",
      "{$set:{address:'$newAddress', city: '$newCity', name: '$newName'', phone: '$newPhone'}}"
    )
  }
}