package com.fvpaz.slashmobilitytest.model

import com.fvpaz.slashmobilitytest.beans.Product
import com.fvpaz.slashmobilitytest.controllers.ProductEdit
import com.fvpaz.slashmobilitytest.model.DbConstants.PRODUCT_COLLECTION
import com.mongodb.client.result.InsertOneResult
import com.mongodb.client.result.UpdateResult
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import org.litote.kmongo.MongoOperator.*
import org.litote.kmongo.coroutine.aggregate
import org.litote.kmongo.eq
import org.litote.kmongo.formatJson


object ProductDAO {
  private val productCollection = Connection().getCollection<Product>(PRODUCT_COLLECTION)

  suspend fun insertProduct(product: Product): InsertOneResult {
    return productCollection.insertOne(product)
  }

  suspend fun editProduct(codProduct: String, product: ProductEdit): UpdateResult {
    val newName = product.name
    val newCompany = product.company
    val newDesc = product.desc
    val newType = product.type
    val newImg = product.imagen

    val editProductBson = """
      
    """.formatJson()

    return productCollection.updateOne(
      "{codProduct: '$codProduct'}",
      "{$set:{desc:'$newDesc', imagen: '$newImg', name: '$newName'', type: '$newType', company: '$newCompany'}}"
    )
  }

  @FlowPreview
  fun listProducts(): Flow<Product> {
    return productCollection.find().toFlow()
  }

  suspend fun getProductByCode(codProduct: String): Product? {
    return productCollection.findOne("{codProduct: '$codProduct'}");
  }

  @FlowPreview
  fun listProductsByType(tipo: String): Flow<Product> {
    return productCollection.find(Product::type eq tipo).toFlow()
  }

  @FlowPreview
  fun listProductsByCity(ciudad: String): Flow<Any> {
    val lookupBson = """
       [
       {$lookup:
            {
              from: "companies",
              localField: "company",
              foreignField: "name",
              pipeline: [{
                   $match: {
                     $expr: {
                       $and: [
                         { $eq: ['$ city', '$ciudad'] }
                       ]
                     }
                   }
                 }
               ],
               as: "Company"
            }
       },{$match: {"Company":{$ne:[]}}}
       ]
       
    """.formatJson()

//    val lookupBson = lookup("companies") //TODO Remplazar por Kmongo implementation
    return productCollection.aggregate<Product>(lookupBson).toFlow()
  }
}