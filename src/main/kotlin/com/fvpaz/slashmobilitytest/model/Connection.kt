package com.fvpaz.slashmobilitytest.model

import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo


const val DB_NAME = "slashMobilityTest"

class Connection {
  private val client = KMongo.createClient().coroutine
  val database = client.getDatabase(DB_NAME)
  val session = GlobalScope.async(start = CoroutineStart.LAZY) { client.startSession() }

  inline fun <reified T : Any> getCollection(collection: String): CoroutineCollection<T> {
    return database.getCollection(collection)
  }
}