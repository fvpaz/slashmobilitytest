package com.fvpaz.slashmobilitytest.model

object DbConstants {
  const val USERS_COLLECTION = "users"
  const val AUTH_COLLECTION  = "auth"
  const val PRODUCT_COLLECTION  = "products"
  const val COMPANY_COLLECTION  = "companies"
}