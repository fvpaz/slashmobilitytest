package com.fvpaz.slashmobilitytest.dao.redis.repository

import com.fvpaz.slashmobilitytest.dao.redis.SessionEntity
import org.springframework.data.repository.CrudRepository
import java.util.*

interface SessionRepo : CrudRepository<SessionEntity, UUID> {
}