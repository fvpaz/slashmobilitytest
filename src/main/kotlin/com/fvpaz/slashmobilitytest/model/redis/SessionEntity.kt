package com.fvpaz.slashmobilitytest.dao.redis

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash
import java.util.*

//@RedisHash("session")
data class SessionEntity(@Id val id: UUID, val user: String)