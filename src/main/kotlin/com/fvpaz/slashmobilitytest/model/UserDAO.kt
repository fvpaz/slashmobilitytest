package com.fvpaz.slashmobilitytest.model

import com.fvpaz.slashmobilitytest.beans.Auth
import com.fvpaz.slashmobilitytest.beans.User
import com.fvpaz.slashmobilitytest.model.DbConstants.AUTH_COLLECTION
import com.fvpaz.slashmobilitytest.model.DbConstants.USERS_COLLECTION
import com.mongodb.client.result.InsertOneResult
import com.mongodb.client.result.UpdateResult
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import org.litote.kmongo.eq
import org.litote.kmongo.setValue

object UserDAO  {
  private val connection by lazy { Connection() }
  private val userCollection = connection.getCollection<User>(USERS_COLLECTION)
  private val authCollection by lazy { connection.getCollection<Auth>(AUTH_COLLECTION) }

  suspend fun addUser(user: User): InsertOneResult {
    return userCollection.insertOne(user)
  }

  suspend fun insertAuth(auth: Auth): InsertOneResult {
    return authCollection.insertOne(auth)
  }

  suspend fun updatePswdByUsername(username: String, newPswd: String): UpdateResult {
    return authCollection.updateOne(Auth::username eq username, setValue(Auth::password, newPswd))
  }

  suspend fun validateUserByCode(codUser: String): UpdateResult {
    return authCollection.updateOne(User::codUser eq codUser, setValue(User::validate, "TRUE"))
//    return authCollection.updateOne("{codUser: '$codUser'}", "{\$set:{validate: 'TRUE'}}")
  }

  suspend fun getUserbyCode(codUser: String): User? {
    return userCollection.findOne(User::codUser eq codUser)
  }

  suspend fun getUserbyID(idUser: String): User? {
    return userCollection.findOne("{_id: ObjectId('$idUser')}")
  }

  suspend fun getAuthData(username: String): Auth? {
    return authCollection.findOne(Auth::username eq username)
  }

  @FlowPreview
  fun listUsers(): Flow<User> {
    return userCollection.find().toFlow()
  }
}
