package com.fvpaz.slashmobilitytest.model

import com.fvpaz.slashmobilitytest.beans.Auth
import com.fvpaz.slashmobilitytest.beans.User
import com.fvpaz.slashmobilitytest.controllers.ResponseData
import com.fvpaz.slashmobilitytest.controllers.Status
import com.fvpaz.slashmobilitytest.model.Constraints.CONSTRAINT_CODE_USER
import com.mongodb.*
import org.litote.kmongo.coroutine.abortTransactionAndAwait
import org.litote.kmongo.coroutine.commitTransactionAndAwait
import com.fvpaz.slashmobilitytest.model.Constraints.CONSTRAINT_USER_EXIST

const val FAIL_INSERT_USER = "No se encontrado el Id del usuario insertado"

class RegisterTransaction {

  suspend fun registerUser(user: User, auth: Auth): ResponseData {
    val connection = Connection()

    val session = connection.session.await()
    val userCollection = connection.getCollection<User>("users")
    val authCollection = connection.getCollection<Auth>("authData")

    return try {
      session.use {
        it.startTransaction(
          TransactionOptions.builder().readPreference(ReadPreference.primary()).readConcern(ReadConcern.LOCAL)
            .writeConcern(WriteConcern.MAJORITY).build()
        )

//        val authResult = authCollection.findOne(Auth::user eq auth.user)      //
//        if (authResult != null)                                              // Solo utilizar cuando no exista un index en la Base de Datos que lo controle.
//          throw RuntimeException("Ya existe este usuario")                  //

        val userResult = userCollection.insertOne(it, user)

        val insertedId = userResult.insertedId ?: throw NullPointerException(FAIL_INSERT_USER)
        auth.userID = insertedId.asObjectId().value.toHexString()

        authCollection.insertOne(it, auth)
        it.commitTransactionAndAwait()

        return ResponseData(Status.SUCCESS, desc = "Succes - Usuario insertado correctamente en la DB")
      }
    } catch (ex: Exception) {
      ex.printStackTrace()
      val responseData = ResponseData(Status.FAIL)
      when (ex) {
        is MongoWriteException -> { //Fallo al insetar un documento en la DB
          val errMsg = ex.error.message
          responseData.desc =
            if (errMsg.startsWith(CONSTRAINT_USER_EXIST)) "Fail DB - Ya existe este usuario"
            else if (errMsg.startsWith(CONSTRAINT_CODE_USER)) "Fail DB - Ya hay un usuario con ese codigo"
            else errMsg
//          else if (errMsg.startsWith(CONSTRAINT_CODE_COMPANY)) "Fail DB - Ya hay un compañia con ese codigo"
//          else if (errMsg.startsWith(CONSTRAINT_CODE_PRODUCT)) "Fail DB - Ya hay un producto con ese codigo"

          return responseData
        }
      }
      session.abortTransactionAndAwait()
      responseData.desc = ex.message ?: "Fail - Error en la transaction"

      return responseData
    } finally {
      session.close()
    }
  }
}

object Constraints {
  const val CONSTRAINT_USER_EXIST =
    "E11000 duplicate key error collection: slashMobilityTest.authData index: user_1 dup key:"
  const val CONSTRAINT_CODE_USER =
    "E11000 duplicate key error collection: slashMobilityTest.users index: codUser_1 dup key:"
  const val CONSTRAINT_CODE_PRODUCT =
    "E11000 duplicate key error collection: slashMobilityTest.products index: codProduct_1 dup key:"
  const val CONSTRAINT_CODE_COMPANY =
    "E11000 duplicate key error collection: slashMobilityTest.companies index: codCompany_1 dup key:"
}

//  fun getNextSequence(name: String?): User? {
//    val searchQuery = BasicDBObject("user_id", "1")
//    val increase = BasicDBObject("seq", 1)
//    val updateQuery = BasicDBObject("\$inc", increase)
//    val result = userCollection.findOneAndUpdate(searchQuery, updateQuery)
//    return result
//  }