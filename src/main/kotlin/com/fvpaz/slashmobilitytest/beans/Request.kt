package com.fvpaz.slashmobilitytest.beans

import kotlinx.serialization.Serializable

@Serializable
data class UserRequest(
  val user: String, val password: String,
  val codUser: String,
  val email: String,
  val name: String = "", val surname: String = ""
)

@Serializable
data class restorPswdRequest(val username: String, val newPswd: String)

//@Serializable
//data class restorPswdRequest(val user: String, val newPswd: String)