package com.fvpaz.slashmobilitytest.beans

import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId

//Entities

@Serializable
data class Auth(
  val username: String,
  val password: String,
  @BsonId var userID: String = ""
)

enum class Validate(val type: String) { UNKNOWN("UNKNOWN"), TRUE("TRUE"), FALSE("FALSE") }

@Serializable
data class User(
  val codUser: String,
  val email: String,
  val name: String = "", val surname: String = "",
  var validate: String = Validate.FALSE.type
)

@Serializable
data class Product(
  val codProduct: String,
  val name: String,
  val type: String,
  val desc: String = "",
  val imagen: String = "",
  val company: String
)

@Serializable
data class Company(
  val codCompany: String,
  val name: String,
  val address: String,
  val phone: String,
  val city: String
)
