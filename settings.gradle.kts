rootProject.name = "SlashmobilityTest"



pluginManagement {
  resolutionStrategy {
    val kotlinVersion = "1.6.10"
    eachPlugin {
      if (requested.id.namespace?.startsWith("org.jetbrains.kotlin") == true) {
        useVersion("1.6.10") //$kotlinVersion
      }
    }
  }
}